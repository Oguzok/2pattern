﻿using System;
using System.Collections.Generic;

abstract class Component
{
    public abstract void Display();
    public abstract Component Clone();
}

class Book : Component
{
    public string Title { get; set; }

    public override void Display()
    {
        Console.WriteLine($"Book: {Title}");
    }

    public override Component Clone()
    {
        return new Book { Title = this.Title };
    }
}

class Magazine : Component
{
    public string Name { get; set; }

    public override void Display()
    {
        Console.WriteLine($"Magazine: {Name}");
    }

    public override Component Clone()
    {
        return new Magazine { Name = this.Name };
    }
}

class Library : Component
{
    public List<Component> components = new List<Component>();

    public void AddComponent(Component component)
    {
        components.Add(component);
    }

    public void RemoveComponent(Component component)
    {
        components.Remove(component);
    }

    public override void Display()
    {
        Console.WriteLine("Library Contents:");
        foreach (var component in components)
        {
            component.Display();
        }
    }

    public override Component Clone()
    {
        var libraryClone = new Library();
        foreach (var component in components)
        {
            libraryClone.AddComponent(component.Clone());
        }
        return libraryClone;
    }
}

class Program
{
    static void Main(string[] args)
    {

        var book1 = new Book { Title = "Book 1" };
        var book2 = new Book { Title = "Book 2" };


        var magazine1 = new Magazine { Name = "Magazine 1" };
        var magazine2 = new Magazine { Name = "Magazine 2" };

        var library = new Library();

        library.AddComponent(book1);
        library.AddComponent(book2);
        library.AddComponent(magazine1);
        library.AddComponent(magazine2);

        library.Display();

        Console.WriteLine();

        var libraryClone = library.Clone() as Library;

        var bookClone = libraryClone.components.Find(c => c is Book && ((Book)c).Title == "Book 1");

        if (bookClone != null)
        {
            libraryClone.RemoveComponent(bookClone);
            Console.WriteLine("Book 1 removed from cloned library.");
        }

        libraryClone.Display();
    }
}
